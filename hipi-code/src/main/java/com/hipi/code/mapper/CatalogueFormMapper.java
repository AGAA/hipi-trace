package com.hipi.code.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hipi.code.domain.CatalogueForm;
import com.hipi.code.domain.query.LogueQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author hipi
 * ClassName:CatalogueFormMapper.java
 * date:2023-01-04 16:28
 * Description:
 */
@Mapper
public interface CatalogueFormMapper extends BaseMapper<CatalogueForm> {

    List<CatalogueForm> getCatalogueByData(LogueQuery logueQuery);
}
