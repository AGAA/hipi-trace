package com.hipi.code.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.hipi.code.domain.dto.BatchCodeExportDTO;
import com.hipi.code.domain.dto.CodeExportDTO;
import com.hipi.code.domain.query.TraceQrQuery;
import com.hipi.code.domain.query.TraceQuery;
import com.hipi.code.domain.vo.TraceCodeVO;
import com.hipi.code.domain.vo.TraceQrLogVO;
import com.hipi.code.service.TraceQrCodeService;
import com.hipi.common.core.controller.BaseController;
import com.hipi.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 二维码表 前端控制器
 * </p>
 *
 * @author hipi
 * @since 2023-01-08
 */
@RestController
@RequestMapping("/code/traceQrCode")
@Api(tags = "生码记录api")
@AllArgsConstructor
public class TraceQrCodeController extends BaseController {

    private final TraceQrCodeService traceQrCodeService;

    /**
     * 分页查询
     *
     * @param page
     * @param traceQrQuery
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public R<IPage<TraceQrLogVO>> page(Page<TraceQrLogVO> page, TraceQrQuery traceQrQuery) {
        return R.ok(traceQrCodeService.selectPage(page, traceQrQuery));
    }

    @SneakyThrows
    @ApiOperation("导出")
    @PostMapping("/export")
    public void export(HttpServletResponse response,@RequestParam(required = false) TraceQrQuery traceQrQuery) {

        String fileName = "二维码-" + ".xlsx";
        String sheetName = "二维码";
        String exportName = URLEncoder.encode(fileName, "utf-8");
        String sheetSubName = URLEncoder.encode(sheetName, "utf-8");
        List<CodeExportDTO> traceCodeVOList = traceQrCodeService.selectListCode(traceQrQuery);
        for (CodeExportDTO dto : traceCodeVOList) {
            String codeUrl = dto.getQrCode();
            //destPath生成的二维码的路径及名称
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            MatrixToImageWriter.writeToStream(
                    new MultiFormatWriter().encode(codeUrl, BarcodeFormat.QR_CODE, 50, 50, hints), "PNG",
                    outputStream);

            dto.setByteArray(outputStream.toByteArray());

        }
        EasyExcel.write(response.getOutputStream(), CodeExportDTO.class).sheet(sheetSubName).doWrite(traceCodeVOList);

    }


}
