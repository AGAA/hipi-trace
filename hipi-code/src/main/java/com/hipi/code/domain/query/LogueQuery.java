package com.hipi.code.domain.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hipi
 * ClassName:ManageQuery.java
 * date:2022-07-19 16:59
 * Description: 查询二维码记录
 */
@Data
@ApiModel(value = "查询", description = "查询")
@NoArgsConstructor
@AllArgsConstructor
public class LogueQuery {

    @ApiModelProperty("数据编号")
    private String dataId;
    @ApiModelProperty("数据类型")
    private String dataType;


}
