package com.hipi.code.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hipi
 * ClassName:TraceQrLogVO.java
 * date:2022-07-23 12:23
 * Description: 生码记录
 */
@Data
@NoArgsConstructor
public class TraceQrLogVO {

    @ApiModelProperty("品类名称")
    @ColumnWidth(70)
    @ExcelProperty("文件名称")
    private String categoryName;

    @ApiModelProperty("批次号")
    @ColumnWidth(70)
    @ExcelProperty("批次号")
    private String batchCode;

    @ApiModelProperty("溯源码")
    private String traceCode;

    @ApiModelProperty("二维码")
    @ColumnWidth(70)
    @ExcelProperty("溯源码")
    private String qrCode;

    @ApiModelProperty("状态")
    private String status;

    @ColumnWidth(20)
    @ExcelProperty("二维码")
    private byte[] byteArray;

}
