package com.hipi.code.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author hipi
 * ClassName:BatchCodeExportDTO.java
 * date:2023-01-17 14:54
 * Description: 批次品类二维码导出
 */
@Data
@ContentRowHeight(100)
public class CodeExportDTO {

    @ApiModelProperty("品类名称")
    @ColumnWidth(70)
    @ExcelProperty("文件名称")
    private String categoryName;

    @ApiModelProperty("批次号")
    @ColumnWidth(70)
    @ExcelProperty("批次号")
    private String batchCode;

    @ApiModelProperty("二维码")
    @ColumnWidth(70)
    @ExcelProperty("溯源码")
    private String qrCode;

    @ColumnWidth(20)
    @ExcelProperty("二维码")
    private byte[] byteArray;
}
